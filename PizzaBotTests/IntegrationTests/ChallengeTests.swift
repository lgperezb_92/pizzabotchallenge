//
//  ChallengeTests.swift
//  PizzaBotTests
//
//  Created by Gael on 16/09/21.
//

import XCTest
@testable import PizzaBot_Challenge
class ChallengeTests: XCTestCase {

    let pizzaBot = PizzaBotChallenge()
    
    func testSimpleDelivery() {
        
        // Happy Path
        var result = pizzaBot.makeDeliveries(for: "5x5(1,3)(4,4)")
    
        switch result.0 {
        case .success(let deliveries):
            XCTAssert(deliveries == "ENNNDEEEND")
            XCTAssert(result.1.count == 0)
        default:
            XCTFail()
        }
        
        result = pizzaBot.makeDeliveries(for: "5x5(0,0)(1,3)(4,4)(4,2)(4,2)(0,1)(3,2)(2,3)(4,1)")
        switch result.0 {
        case .success(let deliveries):
            XCTAssert(deliveries == "DENNNDEEENDSSDDWWWWSDEEENDWNDEESSD")
            XCTAssert(result.1.count == 0)
        default:
            XCTFail()
        }
        
        // Erroneus input
        result = pizzaBot.makeDeliveries(for: "5x(0,0)(1,3)(4,4)(4,2)(4,2)(0,1)(3,2)(2,3)(4,1)")
        switch result.0 {
        case .success(_):
            XCTFail()
        case .failure(let error):
            guard let customError = error as? DBotError else {
                XCTFail()
                return
            }
            XCTAssert(customError == DBotError.invalidGrid)
        }
        
        result = pizzaBot.makeDeliveries(for: "5x4(0,)(1-3)(4,-4)")
        switch result.0 {
        case .success(_):
            XCTFail()
        case .failure(let error):
            guard let customError = error as? DBotError else {
                XCTFail()
                return
            }
            XCTAssert(customError == DBotError.invalidDelivery)
        }
        
        result = pizzaBot.makeDeliveries(for: "5x4(0,0)(6,8)(14,14)")
        switch result.0 {
        case .success(let deliveries):
            XCTAssert(deliveries == "D")
            XCTAssert(result.1.count == 2)
        default:
            XCTFail()
        }
        
        
    }
    
    func testShortestDelivery() {
        // Happy Path
        var result = pizzaBot.makeDeliveriesWithShortPath(for: "5x5(4,4)(1,3)")
    
        switch result.0 {
        case .success(let deliveries):
            XCTAssert(deliveries == "ENNNDEEEND")
            XCTAssert(result.1.count == 0)
        default:
            XCTFail()
        }
        
        result = pizzaBot.makeDeliveriesWithShortPath(for:"5x5(0,0)(1,3)(4,4)(4,2)(4,2)(0,1)(3,2)(2,3)(4,1)")
        // (0,0) - (0,1) - (1,3) - (2,3) - (3,2) - (4,2) - (4,2) - (4,1) - (4,4)
        //   D   -  ND   - ENND  -  ED   -  ESD  -  ED   -   D   -   SD  -  NNND
        //
        switch result.0 {
        case .success(let deliveries):
            XCTAssert(deliveries == "DNDENNDEDESDEDDSDNNND")
            XCTAssert(result.1.count == 0)
        default:
            XCTFail()
        }
        
        result = pizzaBot.makeDeliveriesWithShortPath(for:"5x5(0,0)(1,3)(4,4)(4,2)(4,2)(0,1)(3,2)(2,3)(4,1)(10,1)")
        // (0,0) - (0,1) - (1,3) - (2,3) - (3,2) - (4,2) - (4,2) - (4,1) - (4,4)
        //   D   -  ND   - ENND  -  ED   -  ESD  -  ED   -   D   -   SD  -  NNND
        //
        switch result.0 {
        case .success(let deliveries):
            XCTAssert(deliveries == "DNDENNDEDESDEDDSDNNND")
            XCTAssert(result.1.count == 1)
        default:
            XCTFail()
        }
        
        
    }
    
}
