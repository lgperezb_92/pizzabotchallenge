//
//  PathFinderTests.swift
//  PizzaBotTests
//
//  Created by Gael on 15/09/21.
//

import XCTest
@testable import PizzaBot_Challenge

class PathFinderTests: XCTestCase {

    let pathFinder: PathFinder = SimpleFinder()
   
    func testFinder() {
        
        /// Happy Path
        var spot1 = DeliverySpot(x: 0, y: 0)
        var spot2 = DeliverySpot(x: 1, y: 1)
        
        var path1 = pathFinder.findPath(from: spot1, to: spot2)
        var path2 = pathFinder.findPath(from: spot2, to: spot1)
        
        XCTAssert(path1 == "END")
        XCTAssert(path2 == "WSD")
        
        spot1 = DeliverySpot(x: 0, y: 0)
        spot2 = DeliverySpot(x: 5, y: 3)
        
        path1 = pathFinder.findPath(from: spot1, to: spot2)
        path2 = pathFinder.findPath(from: spot2, to: spot1)
        
        XCTAssert(path1 == "EEEEENNND")
        XCTAssert(path2 == "WWWWWSSSD")
        
        
        spot1 = DeliverySpot(x: 0, y: 0)
        spot2 = DeliverySpot(x: 0, y: 0)
        
        path1 = pathFinder.findPath(from: spot1, to: spot2)
        path2 = pathFinder.findPath(from: spot2, to: spot1)
        
        XCTAssert(path1 == "D")
        XCTAssert(path2 == "D")
        
        spot1 = DeliverySpot(x: 3, y: 1)
        spot2 = DeliverySpot(x: 2, y: 4)
        
        path1 = pathFinder.findPath(from: spot1, to: spot2)
        path2 = pathFinder.findPath(from: spot2, to: spot1)
        
        XCTAssert(path1 == "WNNND")
        XCTAssert(path2 == "ESSSD")
        
    }

}
