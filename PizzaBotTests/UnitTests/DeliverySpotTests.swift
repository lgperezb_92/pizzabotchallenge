//
//  DeliverySpotTests.swift
//  PizzaBotTests
//
//  Created by Gael on 15/09/21.
//

import XCTest
@testable import PizzaBot_Challenge

class DeliverySpotTests: XCTestCase {
    
    func testSpotSet() {
        let d1 = DeliverySpot(x: 0, y: 1)
        let d2 = DeliverySpot(x: 0, y: 5)
        let d3 = DeliverySpot(x: 3, y: 3)
        let d4 = DeliverySpot(x: 4, y: 3)
        let d5 = DeliverySpot(x: 0, y: 1)
        
        var spotSet: Set<DeliverySpot> = Set(arrayLiteral: d1,d2,d3,d4,d5)
        XCTAssert(spotSet.count == 4)
        
        // Priority does not interfer with identity
        spotSet.insert(DeliverySpot(x: 0, y: 5, priority: 1))
        XCTAssert(spotSet.count == 4)
    }
}
