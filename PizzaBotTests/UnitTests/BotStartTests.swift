//
//  BotStartTests.swift
//  PizzaBotTests
//
//  Created by Gael on 16/09/21.
//

import XCTest
@testable import PizzaBot_Challenge

class BotStartTests: XCTestCase {

    func testBotStartSpot() {
        let startSpot = BotStart.startPoint(for: .bottomLeft, in: [[]])
        XCTAssert(startSpot == DeliverySpot(x: 0, y: 0))
    }
}
