//
//  ParserTests.swift
//  PizzaBotTests
//
//  Created by Gael on 15/09/21.
//

import XCTest
@testable import PizzaBot_Challenge

class ParserTests: XCTestCase {

    let parser: StringParser = RegexParser()

    func testGridMaker() {
        
        /// Happy Path
        var input = "5x5"
        var grid = parser.createGrid(from: input)
        XCTAssert(grid?.count == 5)
        XCTAssert(grid?[0].count == 5)
        
        
        /// Non-Square shape
        input = "5x7"
        grid = parser.createGrid(from: input)
        XCTAssert(grid?.count == 7)
        XCTAssert(grid?[0].count == 5)
        
        input = "5x"
        grid = parser.createGrid(from: input)
        XCTAssertNil(grid)
        
        input = "5x2x3"
        grid = parser.createGrid(from: input)
        XCTAssert(grid?.count == 2)
        XCTAssert(grid?[0].count == 5)

        input = "-5x4"
        grid = parser.createGrid(from: input)
        XCTAssertNil(grid)
        
        input = "5xa"
        grid = parser.createGrid(from: input)
        XCTAssertNil(grid)
        
        input = "bx1"
        grid = parser.createGrid(from: input)
        XCTAssertNil(grid)
        
        input = "a*2"
        grid = parser.createGrid(from: input)
        XCTAssertNil(grid)
    }
    
    func testDeliverySpotsMaker() {
        
        /// Happy Path
        var input = "(5,4)(4,4)(3,4)(2,1)"
        var spots = parser.createDeliveriesArray(from: input)
        XCTAssert(spots?.count == 4)
        
        input = "(5,a)(b,b)(11,4)(0,1)"
        spots = parser.createDeliveriesArray(from: input)
        XCTAssert(spots?.count == 2)
        
        input = "(5,a)(b,b)(,4)()"
        spots = parser.createDeliveriesArray(from: input)
        XCTAssertNil(spots)
        
        input = "(5=a)(bxb)(2%4)()"
        spots = parser.createDeliveriesArray(from: input)
        XCTAssertNil(spots)
        
        input = "((4,5)((4,2))(((23))"
        spots = parser.createDeliveriesArray(from: input)
        XCTAssert(spots?.count == 2)
        
        
        input = "(-3,-4)(04,3)"
        spots = parser.createDeliveriesArray(from: input)
        XCTAssert(spots?.count == 1)
        
        input = "(--3,-4)(04,x3)"
        spots = parser.createDeliveriesArray(from: input)
        XCTAssertNil(spots)
    }
}
