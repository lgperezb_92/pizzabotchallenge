//
//  TerminalBot.swift
//  TerminalPizzaBot
//
//  Created by Gael on 19/09/21.
//

import Foundation

enum PathType {
    case fifo
    case shortestNeighbor
    case notSupported
    case quit
    init(string: String) {
        switch string {
        case "s":
            self = .shortestNeighbor
        case "f":
            self = .fifo
        case "q":
            self = .quit
        default:
            self = .notSupported
        }
    }
}

class TerminalBot {
    let console = ConsoleIO()
    let bot = BotInterface()
    
    func start() {

          var shouldQuit = false
          while !shouldQuit {
            
            print("""
            Type:
            'f' for FIFO order
            's' for shortest neighbor
            'q' to quit.
            ->
            """)
            
            let option = getOption(console.getInput())
             
            switch option {
            case .fifo, .shortestNeighbor:
                print("""
                    
                Write input with format: <ℕ>x<ℕ>(<ℕ>,<ℕ>)....
                Example: 4x4(2,1)(2,4)(3,3)
                """)
                
                print(bot.makeDelivery(type: option, input: console.getInput()))
                return
            case .quit:
                shouldQuit = true
            default:
              break
            }
          }
    }
    
    func getOption(_ option: String) -> PathType {
      return PathType(string: option)
    }
}
