//
//  BotInterface.swift
//  TerminalPizzaBot
//
//  Created by Gael on 19/09/21.
//

import Foundation

class BotInterface {
    
    func makeDelivery(type: PathType, input: String) -> String {
        var result: (Result<String, Error>, [DeliverySpot])?
        if type == .fifo {
            result = PizzaBotChallenge().makeDeliveries(for: input)
        } else if type == .shortestNeighbor {
            result = PizzaBotChallenge().makeDeliveriesWithShortPath(for: input)
        } else {
            result = nil
        }
        
        guard let result = result else { return "" }
        switch result.0 {
        case .success(let path):
            return path
        default:
            return ""
        }
        
    }
    
}
