//
//  ConsoleIO.swift
//  TerminalPizzaBot
//
//  Created by Gael on 19/09/21.
//

import Foundation

class ConsoleIO {
    func getInput() -> String {
      let keyboard = FileHandle.standardInput
      let inputData = keyboard.availableData
      let strData = String(data: inputData, encoding: String.Encoding.utf8)!
      return strData.trimmingCharacters(in: CharacterSet.newlines)
    }
}
