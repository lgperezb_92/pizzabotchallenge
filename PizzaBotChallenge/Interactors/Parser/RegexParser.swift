//
//  RegexParser.swift
//  PizzaBot_Challenge
//
//  Created by Gael on 15/09/21.
//

import Foundation

class RegexParser: StringParser {
    
    /**
     Creates a two dimensional array representing the delivery area.
     The array contains 1's meaning route avaiable and 0's for rout blocked.
     
     - Parameter pattern: String with the following format <Number>x<Number> ex. 13x3
     - Returns: 2D array of integers, representing delivery area, marking available routes, nil in case of malformed input.
     */
    func createGrid(from pattern: String) -> [[Int]]? {
        guard let dimensionsPart = findMatches(for: "^[0-9]+x[0-9]+", in: pattern).first else {
            return nil
        }
        
        let dimensions = dimensionsPart.split(separator: "x")
        // Check if dimensions are correct
        guard let x = Int(dimensions[0]), let y = Int(dimensions[1]) else { return nil }
        
        return Array(repeating: Array(repeating: 1, count: x), count: y)
    }
    
    /**
     Creates an array of DeliverySpots using a formatted string.
     
     - Parameter string: String with the following format (Number, Number, Number) ex, (1,3,4)
                         Which represents (x, y, priority). Priority is an optional parameter, default will be 0.
     - Returns: Array of all delivery spots to be visited without filtering, nil in case of mal-formed input.
     */
    func createDeliveriesArray(from string: String) -> [DeliverySpot]? {
        let patterns = findMatches(for: "\\(([^\\)]+)\\)", in: string)
        var spots = [DeliverySpot]()
        
        for pattern in patterns {
            var cleanedPattern = pattern.replacingOccurrences(of: "(", with: "")
            cleanedPattern = cleanedPattern.replacingOccurrences(of: ")", with: "")
            let arrayOfData = cleanedPattern.split(separator: ",")
            guard arrayOfData.count >= 2,
                  let x = Int(arrayOfData[0]), x >= 0,
                  let y = Int(arrayOfData[1]), y >= 0 else {
                continue
            }
            

            let spot = DeliverySpot(x: x, y: y)
            spots.append(spot)
        }
        
        if spots.isEmpty { return nil }
        return spots
    }
    
    /**
     Helper function to match a regex and find the expected formatted string as input
     
     - Parameter regex: Regular Expression definition to match the input expected
     - Parameter text: String to be matched with the regex
     - Returns: Array of matches from input string.
     */
    private func findMatches(for regex: String, in text: String) -> [String] {

        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: text,  range: NSRange(text.startIndex..., in: text))
            return results.map { String(text[Range($0.range, in: text)!]) }
        } catch let error {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
}
