//
//  StringParser.swift
//  PizzaBot_Challenge
//
//  Created by Gael on 15/09/21.
//

import Foundation

/// Defines a Parser that helps to Decode a string to the expected inputs
protocol StringParser {
    func createGrid(from pattern: String) -> [[Int]]?
    func createDeliveriesArray(from patterns: String) -> [DeliverySpot]?
}
