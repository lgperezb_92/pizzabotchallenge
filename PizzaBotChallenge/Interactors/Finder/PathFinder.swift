//
//  PathFinder.swift
//  PizzaBot_Challenge
//
//  Created by Gael on 15/09/21.
//

import Foundation

protocol PathFinder {
    func findPath(from p1: DeliverySpot, to p2: DeliverySpot) -> String
}
