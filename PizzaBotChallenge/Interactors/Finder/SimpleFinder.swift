//
//  SimpleFinder.swift
//  PizzaBot_Challenge
//
//  Created by Gael on 15/09/21.
//

import Foundation

class SimpleFinder: PathFinder {
    
    /**
        Finds a simple route between two points.
    - Parameter p1: Point where agent will start.
    - Parameter p2: Point where agen will arrive.
    - Returns: A string containing a route (W,E,S,N).
     */
    func findPath(from p1: DeliverySpot, to p2: DeliverySpot) -> String {
        let xDiff = p2.xPos - p1.xPos
        let yDiff = p2.yPos - p1.yPos
        
        let directionX = xDiff < 0 ? "W" : "E"
        let directionY = yDiff < 0 ? "S" : "N"
        
        return String(repeating: directionX, count: abs(xDiff)) +
                String(repeating: directionY, count: abs(yDiff)) +
                 "D"
    }
}
