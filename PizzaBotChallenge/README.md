# **Pizza Bot Challenge**

  

## How To Run:
### There are 4 distinct ways to run this project:

*  ### Integration Test:
     - Open Xcode
     - Select 'PizzaBotTests' scheme
     - Navigate to PizzaBotTests/IntegrationTests/ChallengeTests.swift
     - Click on play button at top of class to run on both Fifo and Short Neighbor
     - Copy paste next chunk of code to make your own tests
     -  Replace the String "5x5(1,3)(4,4) with your input"
    - Replace the expected "ENNNDEEEND" result and the count of invalid points in case of any (out of the grid).
   

    Code:
    
    
        switch result.0 {
        case .success(let deliveries):
            XCTAssert(deliveries == "ENNNDEEEND")
            XCTAssert(result.1.count == 0)
        default:
            XCTFail()
        }

- ### Xcode Debugger
    - Open Xcode
    - Select 'Terminal Pizza Bot' scheme
    - Run the project (Play Button)
    - At bottom you will see a console with instruction to test using debugger console.

- ### Terminal using Xcode
    - Open Xcode
    - Select 'Terminal Launcher' Scheme
    - Click Run
    - Terminal should open 
  
- ### Terminal  
    - You must first compile 'Terminal Launcher'
    - This will generate a new file on 'Products' folder
    - Look for TerminalPizzaBot
    - Right-Click, show in finder
    - Drag and drop the file into a terminal


## Interactors

  

****- PathFinder:**** Create directions to go from P1 to P2

****- StringParser:**** Transform a string into a Delivery Area and Spots.

 
## Models

****- Error:**** Defines a basic list of common errors to return

****- DeliverySpot:**** Represents a spot to delivery, allows to grow in components for the future. Main advantage to the  usage of CGPoint

****- BotStart:**** By definition the bot always start at origin (0,0), in case It needs to start at another corner, this would help.

  

## Tests

  

### Integration Test

  

PizzaBotChallenge combines interactors and returns a complet delivery path.

This is an integration test, because It is using directly all other components previously unit tests.

In case of some services like Network, DataBase, Etc... best option is to MockUp interactors.

  

### Unit Tests

  

All code is covered with unit tests, allowing to check for logic each component of the code.

  

- PathFinder

- DeliverySpot

- Parser

- BotStart

  

## SOLID

  

All project has been written following SOLID principles in order to matain the code easy to read and scale.

Interactors conform to protocols to make easy a migration to libraries or other parser/finder like

  

- XML

- JSON

- Fastest Route

- Obstacles

- Among others
