//
//  DeliverySpot.swift
//  PizzaBot_Challenge
//
//  Created by Gael on 15/09/21.
//

import Foundation

/// Represents a point on a 2D space, where a product can be delivered
struct DeliverySpot: Equatable, Hashable {
    
    var xPos: Int
    var yPos: Int
    var priority: Int
    
    init(x: Int, y: Int, priority: Int = 0) {
        self.xPos = x
        self.yPos = y
        self.priority = priority
    }
    
    static func == (lhs: DeliverySpot, rhs: DeliverySpot) -> Bool {
        return lhs.xPos == rhs.xPos &&
            lhs.yPos == rhs.yPos
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(xPos)
        hasher.combine(yPos)
    }
}

