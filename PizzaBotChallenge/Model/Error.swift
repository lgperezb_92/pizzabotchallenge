//
//  Error.swift
//  PizzaBot_Challenge
//
//  Created by Gael on 15/09/21.
//

import Foundation

enum DBotError: Int, Error {
    case invalidGrid = 0
    case invalidDelivery = 1
    case noDelivery = 2
}
