//
//  BotStart.swift
//  PizzaBot_Challenge
//
//  Created by Gael on 16/09/21.
//

import Foundation

// This could grow to support more start points besides origin
enum BotStart {
    case bottomLeft
    
    static func startPoint(for start: BotStart, in grid: [[Int]]) -> DeliverySpot {
        switch start {
        case .bottomLeft:
            return DeliverySpot(x: 0, y: 0)
        }
    }
}
