//
//  PizzaBot_Challenge.swift
//  PizzaBot_Challenge`
//
//  Created by Gael on 14/09/21.
//

import Foundation

class PizzaBotChallenge {

    /// Ideally this could be injected using something like Swinject
    var parser: StringParser = RegexParser()
    var pathFinder: PathFinder = SimpleFinder()
    
    /**
     Creates a route to deliver Pizzas to all clients, describing direction and deliver with following format (W,S,N,E,D)
    - Parameter pattern: Input to be parsed as commands for Delivery Area and Delivery Points.
    - Returns: Success: Delivery path and Not Delivered spots (In case of Any) / Failure
    - Note: Pattern needs to follow next format: <ℕ>x<ℕ>(<ℕ>,<ℕ>)(<ℕ>,<ℕ>)(<ℕ>,<ℕ>).....
    - Warning: In case you don't follow the format, an error will be returned.
     */
    func makeDeliveries(for pattern: String) -> (Result<String,Error>, [DeliverySpot]) {
        guard let deliverySpots = parser.createDeliveriesArray(from: pattern) else {
            return (.failure(DBotError.invalidDelivery), [])
        }
        
        guard let grid = parser.createGrid(from: pattern) else {
            return (.failure(DBotError.invalidGrid), [])
        }
        
        var finalPath = ""
        var startPoint = BotStart.startPoint(for: .bottomLeft, in: grid)
        var erroneusSpots = [DeliverySpot]()
        
        for spot in deliverySpots {
            if isOutside(of: grid, spot: spot) {
                erroneusSpots.append(spot)
                continue
            }
            
            finalPath += pathFinder.findPath(from: startPoint, to: spot)
            startPoint = spot
        }
        
        return (.success(finalPath), erroneusSpots)
    }
    
    func makeDeliveriesWithShortPath(for pattern: String) -> (Result<String,Error>, [DeliverySpot]) {
        guard let deliverySpots = parser.createDeliveriesArray(from: pattern) else {
            return (.failure(DBotError.invalidDelivery), [])
        }
        
        guard let grid = parser.createGrid(from: pattern) else {
            return (.failure(DBotError.invalidGrid), [])
        }
        
        var finalPath = ""
        var startPoint = BotStart.startPoint(for: .bottomLeft, in: grid)
        var allSpots = deliverySpots
        var erroneusSpots = Set<DeliverySpot>()
        
        while !allSpots.isEmpty {
            var minDistance: Int = Int.max
            var currentPath = ""
            var winnerIndex = 0
            for (i,spot) in allSpots.enumerated() {
                if isOutside(of: grid, spot: spot) {
                    erroneusSpots.insert(spot)
                    continue
                }
                
                let delivery = pathFinder.findPath(from: startPoint, to: spot)
                if delivery.count < minDistance {
                    minDistance = delivery.count
                    currentPath = delivery
                    winnerIndex = i
                }
            }
            
            finalPath += currentPath
            startPoint = allSpots[winnerIndex]
            allSpots.remove(at: winnerIndex)
        }
        
        return (.success(finalPath), Array(erroneusSpots))
    }

    /**
     Check if a spot is inside the Delivery Area.
     */
    private func isOutside(of grid: [[Int]], spot: DeliverySpot) -> Bool {
        return spot.yPos > grid.count ||
            spot.xPos > grid[0].count
    }
    
}

